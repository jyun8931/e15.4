import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class E15_4 {

    public static void main(String[] args) {
        // create a map with key and value as string
        Map <String, String> studentMap = new HashMap<String, String>();
        //create object to scanner class to read input from user
        Scanner input = new Scanner(System.in);
        String userChoice, name, grade;
        boolean repeat = true;
        //repeat until user wishes to quit
        while(repeat)
        {
            //display the menu
            System.out.println("1. Add Student");
            System.out.println("2. Remove Student");
            System.out.println("3. Display");
            System.out.println("4. Modify");
            System.out.println("5. Quit");
            //prompt and read the user choice
            System.out.print("Enter your choice: ");
            userChoice = input.nextLine();
            //run a switch case based on user choice
            switch(userChoice)
            {
                //if user entered 1
                case "1":
                    //prompt and read student name to be added
                    System.out.print("\nEnter student name: ");
                    name = input.nextLine();
                    //prompt and read grade
                    System.out.print("Enter grade received: ");
                    grade = input.nextLine();
                    //add the student and his grade to the map
                    studentMap.put(name, grade);
                    break;
                //if user entered 2
                case "2":
                    //prompt and read student to be removed
                    System.out.print("\nEnter student name to be removed: ");
                    name = input.nextLine();
                    //remove the student
                    studentMap.remove(name);
                    break;
                //if user entered 3
                case "3":
                    System.out.println();
                    //create a tree map inorder to get the map sorted
                    Map<String, String> treeMap = new TreeMap<String, String>(studentMap);
                    //iterate and display the map
                    for(Map.Entry<String, String> m:treeMap.entrySet())
                    {
                        System.out.println(m.getKey() + ": "+m.getValue());
                    }
                    break;
                //if user entered 4
                case "4":
                    System.out.print("\nEnter Students name to modified: ");
                    name = input.nextLine();
                    System.out.print("\nEnter the Student's new grade: ");
                    grade = input.nextLine();
                    //update student
                    studentMap.put(name, grade);
                    break;
                //if user entered 5
                case "5":
                    //set repeat to false to terminate the loop
                    repeat = false;
                    //display terminating message
                    System.out.println("Program terminated..");
                    break;
                //display error message if user entered anything else
                default:
                    System.out.println("Invalid choice.");
            }
            System.out.println();
        }
        input.close();
    }
}
